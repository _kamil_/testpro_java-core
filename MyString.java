/**
 * Test automation - week 2
 * Task 1 - Prints the result: if the string is even/not even
 * task 2 - Prints the result: if a string is palindrome/not palindrome
 */




import java.util.Scanner;

public class MyString {
    String str;

    // Assigns user input string to an object's attribute
    public void getUserInput() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input a string please: ");
        this.str = keyboard.nextLine();
    }


    // Task 1 - Returns true, if  number num is even, false - otherwise.
    public static boolean isEven(int num) {
        return (num%2 == 0);
    }

    // Task 1 - Prints the result: if the string is even/not even
    public void printStringType() {
        if (isEven(this.str.length()))
            System.out.println("String is even");
        else
            System.out.println("String is not even");
    }

    // Task 2 - Returns true, if a string is palindrome, false - otherwise
    public boolean isPalindrome() {

        // Copy of current string
        String tmpString = this.str;

        // Getting rid of all symbols but characters and digits
        // tmpString - new temporary string without extra symbols to iterate thru
        tmpString = tmpString.replaceAll("[^a-zA-Z0-9]+", "").toLowerCase();
        int len = tmpString.length();

        // Checks if characters of a string on the left and right sides are not equal.
        // If not - returns false, if yes - continue till reaching the middle element(s)
        for (int i=0; i <= len/2; i++) {
            if (tmpString.charAt(i) != tmpString.charAt(len-i-1))
                return false;
        }
        return true;
    }

    // Task 2 - Prints the result: if a string is palindrome/not palindrome
    public void printPalindrome() {
        if (isPalindrome())
            System.out.println("This string is palindrome");
        else
            System.out.println("This string is not palindrome");
    }

    public static void main(String[] var0) {
        MyString myStr = new MyString();
        myStr.getUserInput();
        myStr.printStringType();
        myStr.printPalindrome();
    }
}



