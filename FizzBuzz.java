/**
 * Test automation - week 3 - FizzBuzz. Task 1 - using a for loop. Task 2 - using recursion
 */


import java.util.Scanner;

public class FizzBuzz {

    // Returns user input number
    private static int getUserInput() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input a boundary of FizzBuzz test please: ");
        return keyboard.nextInt();
    }

    // Task 1 - FizzBuzz using a for loop.
    // Prints th results for all numbers less or equal to boundary.
    private static void printFizzBuzz(int boundary) {

        for (int i = 1; i <= boundary; i++) {
            if (i%15 == 0) {
                System.out.println("FizzBuzz");
            } else if (i%3 == 0) {
                System.out.println("Fizz");
            } else if (i%5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    // Task 2 - FizzBuzz using recursion.
    // Prints th results for all numbers less or equal to boundary.
    private static void printFizzBuzzRec(int num, int boundary) {

        if (num%15 == 0) {
            System.out.println("FizzBuzz");
        } else if(num%3 == 0) {
            System.out.println("Fizz");
        } else if(num%5 == 0) {
            System.out.println("Buzz");
        } else {
            System.out.println(num);
        }

        // Exit from recursion
        if (num == boundary) {
            return;
        // Recursion
        } else {
            printFizzBuzzRec(num + 1, boundary);
        }
    }

    public static void main(String[] args) {
        int boundary = getUserInput();
        FizzBuzz fb = new FizzBuzz();
        System.out.println("Using for loop: ");
        fb.printFizzBuzz(boundary);
        System.out.println("\nUsing recursion: ");
        fb.printFizzBuzzRec(1, boundary);
    }
}